.PHONY: tests

CC = g++
STD = -std=c++20
INCLUDE_PATHS = -Isrc
#LDLIBS =  -lvulkan -lglfw -ldl -lpthread
#LIBSPATH = -Lbuild
#DEFINITIONS = -DRENDER_DEBUG

tests: tests/test.cpp
	$(CC) $(STD) -ggdb tests/test.cpp $(DEFINITIONS) $(LIBSPATH) $(CFILES) $(INCLUDE_PATHS) $(LDLIBS) -o build/test
	./build/test

clean:
	rm -f *.o *.a

