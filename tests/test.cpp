#include "jath.hpp"

#include <cassert>
#include <cstdio>
#include <limits>

auto test1_vec_arithmethic() -> void;
auto test2_vec_dot() -> void;
auto test3_mat_transpose() -> void;
auto test3_mat_mul() -> void;
auto test4_mat_perpective() -> void;
auto test5_mat_lookat() -> void;
auto test6_mat_add() -> void;

auto main () -> int
{
    test1_vec_arithmethic();
    test2_vec_dot();
    test3_mat_transpose();
    test3_mat_mul();
    test4_mat_perpective();
    test5_mat_lookat();
    test6_mat_add();
    return 0;
}

auto test1_vec_arithmethic() -> void
{
    jth::vec3 v1 = {5, 6.5f, 2};
    jth::vec3 v2 = {2, 1, 0.5f};

    v1[0];

    auto add = v1 + v2;
    auto add2 = v2 + v1;

    assert(add->x == add2->x && add->y == add2->y && add->z == add2->z);
    assert(add->x == 7 && add->y == 7.5f && add->z == 2.5f);

    auto sub = v1 - v2;
    assert(sub->x == 3 && sub->y == 5.5f && sub->z == 1.5f);

    auto mul = v1 * v2;
    auto mul2 = v2 * v1;

    assert(mul->x == mul2->x && mul->y == mul2->y && mul->z == mul2->z);
    assert(mul->x == 10 && mul->y == 6.5f && mul->z == 1);

    printf("[SUCCESS]\t\ttest_vec_arithmethic\n");
}

auto test2_vec_dot() -> void
{
    jth::vec3 v1 = {5, 6.5f, 2};
    jth::vec3 v2 = {2, 1, 0.5f};

    auto dot1 = jth::dot(v1, v2);
    auto dot2 = jth::dot(v2, v1);

    assert(dot1 == dot2);
    assert(dot1 == 17.5f);

    printf("[SUCCESS]\t\ttest_vec_dot\n");
}

auto test3_mat_transpose() -> void
{
    jth::mat3 m1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8};

    jth::mat3 t = m1.transpose();

    for (size_t r = 0; r < 3; r++)
        for (size_t c = 0; c < 3; c++)
            assert(m1[r][c] == t[c][r]);

    printf("[SUCCESS]\t\ttest3_mat_transpose\n");
}

auto test3_mat_mul() -> void
{
    jth::mat<float, 2, 4> m1 = {{{2, 6}, {-3, 8}, {5, 9}, {9, 2}}};
    jth::mat<float, 4, 3> m2 = {5, -9, 4, 8, 3, 5, 0, 2, 2, 0, 1, 1};
   
    auto res = m1 * m2;
   
    assert(res[0][0] == 129);
    assert(res[0][1] == 10);
    assert(res[1][0] == 9);
    assert(res[1][1] == 62);
    assert(res[2][0] == 18);
    assert(res[2][1] == 23);

    printf("[SUCCESS]\t\ttest3_mat_mul\n");
}

auto test4_mat_perpective() -> void
{
    auto persp = jth::persp_proj_rh(3.14f/2.0f, 50.0f, 100.0f, 0.01f, 100.0f);
    jth::mat4 exp = 
    {
        2.001593f, 0, 0, 0, 
        0, -1.000797f, 0, 0,
        0, 0, -1.0001f, -1.0f,
        0, 0, -0.010001f, 0
    };
    assert(jth::equal(persp, exp, 0.0001f));
    printf("[SUCCESS]\t\ttest4_mat_perspective\n");
}

auto test5_mat_lookat() -> void
{
    auto rot = jth::look_at_rh(jth::vec3{1.0f, 0.0f, 0.0f}, 
            jth::vec3{0.0f, 0.0f, 2.0f}, jth::vec3{0.0f, 1.0f, 0.0f});
    jth::mat4 exp = 
    {
            -0.89442730, 0.00000000, 0.44721359, 0.00000000, 
            0.00000000, 1.00000012, -0.00000000, 0.00000000,
            -0.44721365, 0.00000000, -0.89442718, 0.00000000,
            0.89442730, -0.00000000, -0.44721359, 1.00000000
    };

    assert(jth::equal(rot, exp, 0.0001f));
    printf("[SUCCESS]\t\ttest5_mat_lookat\n");
}

auto test6_mat_add() -> void
{
    jth::mat<int, 2, 3> m1 = {{{1, 2}, {3, 4}, {5, 6}}};
    jth::mat<int, 2, 3> m2 = {{{1, 2}, {3, 4}, {5, 6}}};
    jth::mat<int, 2, 3> ex = {{{2, 4}, {6, 8}, {10, 12}}};
    auto res = m1 + m2;

    assert(jth::equal(ex, res));
    printf("[SUCCESS]\t\ttest6_mat_add\n");
}
